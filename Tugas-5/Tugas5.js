// soal 1

function teriak() {
  return "Halo Sanbers!";
}

console.log("soal 1");
console.log();

console.log(teriak());

console.log();

// soal 2

function kalikan(a, b) {
  return a * b;
}

var num1 = 12;
var num2 = 4;

console.log("soal 2");
console.log();

var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

console.log();

// soal 3

function introduce(name, age, address, hobby) {
  return (
    "Nama saya " +
    name +
    ", umur saya " +
    age +
    " tahun, alamat saya di " +
    address +
    ", dan saya punya hobby yaitu " +
    hobby +
    "!"
  );
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

console.log("soal 3");
console.log();

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"

console.log();

// soal 4

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];

var objPesertaLama = {
  nama: arrayDaftarPeserta[0],
  "jenis kelamin": arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  "tahun lahir": arrayDaftarPeserta[3],
};

var objPeserta = {
  nama: arrayDaftarPeserta[0],
  jenisKelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunLahir: arrayDaftarPeserta[3],
};

console.log("soal 4");
console.log();

console.log(objPeserta);

console.log();

// soal 5

var arrBuahLama = [
  { nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000 },
  { nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000 },
  {
    nama: "semangka",
    warna: "hijau & merah",
    "ada bijinya": "ada",
    harga: 10000,
  },
  { nama: "pisang", warna: "kuning", "ada bijinya": "tidak", harga: 5000 },
];

var arrBuah = [
  { nama: "strawberry", warna: "merah", adaBijinya: false, harga: 9000 },
  { nama: "jeruk", warna: "oranye", adaBijinya: true, harga: 8000 },
  {
    nama: "semangka",
    warna: "hijau & merah",
    adaBijinya: true,
    harga: 10000,
  },
  { nama: "pisang", warna: "kuning", adaBijinya: false, harga: 5000 },
];

console.log("soal 5");
console.log();

console.log(arrBuah[0]);

console.log();

// soal 6

var dataFilm = [];

function addFilm(nama, durasi, genre, tahun) {
  var film = {
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun,
  };
  dataFilm.push(film);
  console.log(dataFilm);
}

console.log("soal 6");
console.log();

addFilm("Avenger", "2 jam", "Action", 2015);
addFilm("LOTR", "4 jam", "Fantasi", 2013);
