// soal 1

let mulai = 0;
let mulai2 = 20;

console.log("soal 1");

console.log();

console.log("LOOPING PERTAMA");
while (mulai <= 20) {
  if (mulai % 2 === 0 && mulai !== 0) {
    console.log(`${mulai} - I love coding`);
  }
  mulai++;
}

console.log("LOOPING KEDUA");
while (mulai2 >= 0) {
  if (mulai2 % 2 === 0 && mulai2 !== 0) {
    console.log(`${mulai2} - I will become a mobile developer`);
  }
  mulai2--;
}

// soal 2

console.log("soal 2");

console.log();

for (let i2 = 1; i2 < 21; i2++) {
  if (i2 % 2 === 1 && i2 % 3 === 0) {
    console.log(`${i2} - I Love Coding`);
  } else if (i2 % 2 === 1) {
    console.log(`${i2} - Santai`);
  } else if (i2 % 2 === 0) {
    console.log(`${i2} - Berkualitas`);
  } else null;
}

console.log();

// soal 3

console.log("soal 3");

console.log();

const tinggi = 7;
const alas = 7;
let tampung4 = "";

for (let i4 = 0; i4 < tinggi; i4++) {
  for (let j4 = 0; j4 <= i4; j4++) {
    tampung4 = tampung4 + "#";
  }
  console.log(tampung4);
  tampung4 = "";
}

console.log();

// soal 4

console.log("soal 4");

console.log();

var kalimat = "saya sangat senang belajar javascript";

let tampArr = kalimat.split(" ");

console.log(tampArr);

console.log();

// soal 5

console.log("soal 5");

console.log();

var daftarBuah = [
  "2. Apel",
  "5. Jeruk",
  "3. Anggur",
  "4. Semangka",
  "1. Mangga",
];

let urutBuah = daftarBuah.sort();

for (let i5 = 0; i5 < urutBuah.length; i5++) {
  console.log(urutBuah[i5]);
}

console.log();
