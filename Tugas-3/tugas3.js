// soal 1

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

kataKedua = kataKedua.substring(0, 1).toUpperCase() + kataKedua.substring(1, 6);

const kalimat =
  kataPertama +
  " " +
  kataKedua +
  " " +
  kataKetiga +
  " " +
  kataKeempat.toUpperCase();

console.log("=== soal 1 ===");

console.log();

console.log(kalimat);

console.log();

// soal 2

var kataPertama2 = "1";
var kataKedua2 = "2";
var kataKetiga2 = "4";
var kataKeempat2 = "5";

const jumlah =
  parseInt(kataPertama2) +
  parseInt(kataKedua2) +
  parseInt(kataKetiga2) +
  parseInt(kataKeempat2);

console.log("=== soal 2 ===");

console.log();

console.log(jumlah);

console.log();

// soal 3

var kalimat3 = "wah javascript itu keren sekali";

var kataPertama3 = kalimat3.substring(0, 3);
var kataKedua3 = kalimat3.substring(4, 14);
var kataKetiga3 = kalimat3.substring(15, 18);
var kataKeempat3 = kalimat3.substring(19, 24);
var kataKelima3 = kalimat3.substring(25, 31);

console.log("=== soal 3 ===");

console.log();

console.log("Kata Pertama: " + kataPertama3);
console.log("Kata Kedua: " + kataKedua3);
console.log("Kata Ketiga: " + kataKetiga3);
console.log("Kata Keempat: " + kataKeempat3);
console.log("Kata Kelima: " + kataKelima3);

console.log();

// soal 4

var nilai;

nilai = 75;

console.log("=== soal 4 ===");

console.log();

if (nilai >= 0 && nilai <= 100) {
  nilai >= 80
    ? console.log("index nilainya A")
    : nilai >= 70
    ? console.log("index nilainya B")
    : nilai >= 60
    ? console.log("index nilainya C")
    : nilai >= 50
    ? console.log("index nilainya D")
    : console.log("index nilainya E");
} else {
  console.log("nilai yang dimasukkan diluar rentang nilai 0 - 100");
}

console.log();

// soal 5

var tanggal = 27;
var bulan = 3;
var tahun = 1989;

console.log("=== soal 5 ===");

console.log();

switch (bulan) {
  case 1:
    console.log(tanggal + " Januari " + tahun);
    break;
  case 2:
    console.log(tanggal + " Februari " + tahun);
    break;
  case 3:
    console.log(tanggal + " Maret " + tahun);
    break;
  case 4:
    console.log(tanggal + " April " + tahun);
    break;
  case 5:
    console.log(tanggal + " Mei " + tahun);
    break;
  case 6:
    console.log(tanggal + " Juni " + tahun);
    break;
  case 7:
    console.log(tanggal + " Juli " + tahun);
    break;
  case 8:
    console.log(tanggal + " Agustus " + tahun);
    break;
  case 9:
    console.log(tanggal + " September " + tahun);
    break;
  case 10:
    console.log(tanggal + " Oktober " + tahun);
    break;
  case 11:
    console.log(tanggal + " Nopember " + tahun);
    break;
  case 12:
    console.log(tanggal + " Desember " + tahun);
    break;
  default:
    console.log("nilai bulan diluar rentang 1 - 12");
    break;
}

console.log();
