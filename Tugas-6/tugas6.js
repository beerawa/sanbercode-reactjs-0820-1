// Soal 1

const luasLingkaran = (jari) => {
  let luas = (22 / 7) * jari * jari;
  return luas;
};

const kelilingLingkaran = (jari) => {
  let keliling = (22 / 7) * 2 * jari;
  return keliling;
};

console.log("Soal 1");
console.log();

console.log("Luas Lingkaran");
console.log(luasLingkaran(7));
console.log(luasLingkaran(9));
console.log();

console.log("Keliling Lingkaran");
console.log(kelilingLingkaran(7));
console.log(kelilingLingkaran(9));
console.log();

// Soal 2

let kalimat = "";
let kalimat2 = "";

let kata1 = "saya";
let kata2 = "adalah";
let kata3 = "seorang";
let kata4 = "frontend";
let kata5 = "developer";

const rangkaiKata = (a, b, c, d, e) => {
  kalimat = `${a} ${b} ${c} ${d} ${e}`;
  return kalimat;
};

const rangkaiKata2 = (...arg) => {
  kalimat2 = arg;
  return kalimat2.join(" ");
};

console.log("Soal 2");
console.log();

console.log(rangkaiKata(kata1, kata2, kata3, kata4, kata5));
// console.log(rangkaiKata2("saya", "adalah", "seorang", "frontend", "developer"));

console.log();

// Soal 3

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: function () {
      console.log(firstName + " " + lastName);
      return;
    },
  };
};

console.log("Soal 3");
console.log();
newFunction("William", "Imoh").fullName();
console.log();

// Soal 4

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};

const { firstName, lastName, destination, occupation, spell } = newObject;

console.log("Soal 4");
console.log();
console.log(firstName, lastName, destination, occupation);
console.log();

// Soal 5

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// const combined = west.concat(east);
const combined = [...west, ...east];
//Driver Code

console.log("Soal 5");
console.log();
console.log(combined);
