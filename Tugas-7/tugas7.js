// soal 1

class Animal {
  // Code class di sini
  constructor(name) {
    this.name = name;
    this.legs = 4;
    this.cold_blooded = false;
  }
}

var sheep = new Animal("shaun");

console.log("soal 1 Release 0");
console.log();

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

console.log();

// Code class Ape dan class Frog di sini

class Ape extends Animal {
  constructor(name) {
    super(name);
    this.legs = 2;
  }
  yell() {
    console.log("Auooo");
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
  }
  jump() {
    console.log("hop hop");
  }
}

console.log("soal 1 Release 1");
console.log();

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

console.log();

// console.log(sungokong.name);
// console.log(sungokong.legs);
// console.log(kodok.name);
// console.log(kodok.legs);

// 2. soal 2

// function Clock({ template }) {
//   var timer;

//   function render() {
//     var date = new Date();

//     var hours = date.getHours();
//     if (hours < 10) hours = "0" + hours;

//     var mins = date.getMinutes();
//     if (mins < 10) mins = "0" + mins;

//     var secs = date.getSeconds();
//     if (secs < 10) secs = "0" + secs;

//     var output = template
//       .replace("h", hours)
//       .replace("m", mins)
//       .replace("s", secs);

//     console.log(output);
//   }

//   this.stop = function () {
//     clearInterval(timer);
//   };

//   this.start = function () {
//     render();
//     timer = setInterval(render, 1000);
//   };
// }

// var clock = new Clock({ template: "h:m:s" });
// clock.start();

class Clock {
  // Code di sini
  constructor({ template }) {
    this.timer = 0;
    this.template = template;
  }
  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    var output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    // console.log(this.template);
    this.render();
    this.timer = setInterval(this.render.bind(this), 1000);
    // this.timer = setInterval(function () {
    //   this.render;
    // }, 1000);
  }
}

console.log("soal 2");
console.log();

var clock = new Clock({ template: "h:m:s" });
clock.start();
// console.log(clock.template);
