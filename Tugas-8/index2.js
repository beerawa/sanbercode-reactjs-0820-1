var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise

let rentang = 10000;
let a = 0;

(async function loop() {
  for (let i = 0; i < books.length; i++) {
    await readBooksPromise(rentang, books[a])
      .then((response) => {
        rentang = response;
        // if (rentang < books.timeSpent[a + 1]) {
        //   console.log("Sisa waktu saya hanya " + rentang);
        // }
      })
      .catch(function (err) {
        console.log("Sisa waktu saya hanya " + err);
      });
    a++;
  }
})();
