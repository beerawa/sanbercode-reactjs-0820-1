// di file promise.js
function readBooksPromise(time, book) {
  console.log(`saya mulai membaca ${book.name}`);
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      // console.log("time: " + time);
      // console.log("book " + book);
      let sisaWaktu = time - book.timeSpent;
      // console.log("Sisawaktu: " + sisaWaktu);
      if (sisaWaktu >= 0) {
        console.log(
          `saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisaWaktu}`
        );
        rentang = sisaWaktu;
        // console.log(rentang);
        resolve(sisaWaktu);
      } else {
        console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`);
        sisaWaktu = rentang;
        reject(sisaWaktu);
      }
    }, book.timeSpent);
  });
}

module.exports = readBooksPromise;
