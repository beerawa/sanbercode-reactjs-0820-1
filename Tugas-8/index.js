// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Tulis code untuk memanggil function readBooks di sini
// readBooks(10000, books[0], function (waktu) {

// });

let rentang = 10000;
// let waktuku = 0;
let a = 0;

function proses(waktu) {
  if (a < books.length - 1 && waktu > 0) {
    if (waktu !== rentang) {
      rentang = waktu;
      // waktuku = rentang;
      // console.log(waktu);
      a++;
      readBooks(rentang, books[a], proses);
    } else console.log("sisa waktu : " + waktu);
  } else console.log("sisa waktu : " + waktu);
}

readBooks(rentang, books[a], proses);
